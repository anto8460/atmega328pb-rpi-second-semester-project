# **Second Semester Project During LockDown**

## 1. **Introduction**

- This document describes the creation of a system that accepts hardware inputs from push buttons and turns ON/OFF peripheral LEDs, communicates over UART and TCP/IP sockets to transmit information between components. At the end of the system there is a dashboard that can control all peripherals like LEDs and PWM output as well as monitor Analog inputs.

- Components used for this project:

    - 2 Transistors 2n3904
    - 3 LEDS
    - 6 100 ohm resistors
    - 2 330 ohm resistors
    - 2 push buttons
    - 2 breadboards
    - jumper wires
    - DC fan
    - H-bridge
    - 1k potentiometer
    - ATmega328pb Xplained Board
    - Raspberry Pi 3b+
    - Personal Laptop

## **2. Block Diagram of the system**

![](BlockDiagram.PNG)

## **3. Interface Module setup**

![](Interface_Module.PNG)

**Notes:**

- The resistors connected to the 2 LEDs are 330 ohms
- The resistors connected to the Bases of the thransistors are 100 ohms

## **4. H-bridge Setup**

![](H-Bridge.PNG)

## **5. ADC pin Connection**

![](potent.PNG)

## **6. ATmega software flow chart**

![](FlowChart_Atmega.PNG)

[Link to Codes](https://gitlab.com/anto8460/hm-emb-sys-ww36/-/tree/master/Second%20Semester/Second%20Semester%20Project/Atmega328pb%20codes)

## **7. Raspberry Pi software flow chart**

![](RPi_flow.PNG)

[Link to Codes](https://gitlab.com/anto8460/hm-emb-sys-ww36/-/tree/master/Second%20Semester/Second%20Semester%20Project/Raspberry%20Pi%20codes)

## **8. Communication Protocol**

### **Messages are going to be constructed using 3 main parts:**

**1. Data** - this part is going to contain a value like 0(False) or 1 (True), ADC measurement in bits, or duty cycle in bits

**2. Peripheral** - this part is responsible for determining the peripheral that is supposed to recieve or send the corresponding data. Peripherals might be the LEDs on the interface, the ADC pin and the PWM

**3. Number** - this part is used when there are multiple peripheral of one type. In this system the only matching periferals are the two LEDs, however the system can be expanded to use more than one PWM or ADC chanel

### **Table of messages used in this system:**

|   Name	|   Data	|   Peripheral	|   Number	|
|---	|---	|---	|---	|
|   LED1	|   000-001	|   1	|   1	|
|   LED2	|   000-001	|   1	|   2	|
|   ADC	|   0-1024	|   2	|   1	|
|   PWM	|   0-255	|   3	|   1	|

### **Example:**
- If LED1 needs to be turned ON, a message of >> **00111** is send
- If LED2 needs to be turned OFF, a message of >> **00012** is send

**Note:**

- When sending a Duty Cycle which is less than 3 digits the message should still be of 5 bytes. For example:
    
    - Duty Cycle == 90, then the message should be **09031**

### **ADC requests**

- Since ADC sends information of the sensor only one way, when requesting data a message of : **00021** is send.
- The return is **(0-1023)21**

### **PWM messages**

- Since PWM is configured only to accept values it does not return the set Duty Cycle value

## **8. Node-Red nodes Setup**

![](Node-Red-Setup.PNG)

- **Set Duty Cycle Node**

![](Set_Duty_Cycle.PNG)

- **Convert % >> bits node**

![](Convert_percents_to_bits.PNG)

- This node converts the X*% duty cycle to X*bits duty cycle

- **Convert_PWM node**

![](Convert_PWM.PNG)

 - This function node makes sure that the message send to the RPi and the to the ATmega is 5 bytes

- **LED1-ON/OFF**

![](LED1_ON-OFF.PNG)

- **LED2-ON/OFF**

![](LED2_ON-OFF.PNG)

- **TCP node**

![](TCP-node.PNG)

- This node is responsible for TCP/IP communication 
between RPi and Personal Computer

- **toString node**

![](toString.PNG)

- Converts the Arrays of bytes recieved by the TCP node to strings

- **Switch Node**

![](Switch.PNG)

- Directs the different messages to different nodes according to their value

- **Decoding 1**

![](Decoding_1.PNG)

- Translates incomming massage to a more readable form

- **Decoding 2**

![](Decoding_2.PNG)

- Translates incomming massage to a more readable form

- **LED1 node**

![](LED1.PNG)

- a Label to display the state of the LED1

- **LED2 node**

![](LED2.PNG)

- a Label to display the state of the LED2

- **Decoding ADC 1 node**

![](Decoding_ADC.PNG)

- Since ADC value can be from 0-1023 this node removes any empty spaces which are conviniently set to 'o' in the ATmega328pb code.

- **Decoding ADC 2 node**

![](Decoding_ADC_2.PNG)

- This node converts the recieved value of the ADC in bits to a value betwee 0 and 5V.

- **ADC node**

![](ADC_gauge.PNG)

- This node displays the ADC value

## **8.Dashboard**

![](Dashboard.PNG)


## **9. Link to Youtube Video**

https://youtu.be/tcPQIGSMqZM