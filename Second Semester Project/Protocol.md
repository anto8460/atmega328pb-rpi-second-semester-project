# **Protocol for Sending and Receiving data in the System**

### **Messages are going to be constructed using 3 main parts:**

**1. Data** - this part is going to contain a value like 0(False) or 1 (True), ADC measurement in bits, or duty cycle in bits

**2. Peripheral** - this part is responsible for determining the peripheral that is supposed to recieve or send the corresponding data. Peripherals might be the LEDs on the interface, the ADC pin and the PWM

**3. Number** - this part is used when there are multiple peripheral of one type. In this system the only matching periferals are the two LEDs, however the system can be expanded to use more than one PWM or ADC chanel

### **Table of messages used in this system:**

|   Name	|   Data	|   Peripheral	|   Number	|
|---	|---	|---	|---	|
|   LED1	|   000-001	|   1	|   1	|
|   LED2	|   000-001	|   1	|   2	|
|   ADC	|   0-1024	|   2	|   1	|
|   PWM	|   0-255	|   3	|   1	|

### **Example:**
- If LED1 needs to be turned ON, a message of >> **00111** is send
- If LED2 needs to be turned OFF, a message of >> **00012** is send

**Note:**

- When sending a Duty Cycle which is less than 3 digits the message should still be of 5 bytes. For example:
    
    - Duty Cycle == 90, then the message should be **09031**

### **ADC requests**

- Since ADC sends information of the sensor only one way, when requesting data a message of : **00021** is send.
- The return is **(0-1023)21**

### **PWM messages**

- Since PWM is configured only to accept values it does not return the set Duty Cycle value