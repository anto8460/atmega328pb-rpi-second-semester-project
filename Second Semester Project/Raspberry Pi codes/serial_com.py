# This file contains the function responsible for UART transission and receiving

import serial
from time import sleep

ser = serial.Serial('/dev/ttyACM0', 9600, timeout=0.5)


def getADC():                                             # Request the ADC value over UART
	
	while True:

		ser.write(b'00021')                               # Send specific message of 5 bytes
		data = ser.readline()                             # Loop until data is received
		if data == '':
			continue
		else:
			break
	return data


def LED1_func(state):                                     # Change the state of LED 1

	if (state == 0):                                      # If the user wants LED 1 off:

		while True:
			ser.write(b'00011')                           # Send necessary message
			data = ser.readline()                         # Read and store the new LED status
			if data == '':
				continue
			else:
				break



	elif (state == 1):                                    # If the user want LED 1 on:

		while True:                     
			ser.write(b'00111')                           # Send the necessary message
			data = ser.readline()                         # Read and store the new LED status
			if data == '':
				continue
			else:
				break

	return data

def LED2_func(state):                                     # Change the state of LED 2

	if (state == 0):

		while True:

			ser.write(b'00012')
			data = ser.readline() 
			if data == '':
				continue
			else:
				break

	elif (state == 1):

		while True:
			ser.write(b'00112')
			data = ser.readline() 
			if data == '':
				continue
			else:
				break

	return data

def setPWM(duty_cycle):                                     # Send a duty cycle

	for i in range(4):

		str_duty = str(duty_cycle)                          # Convert integer to str
		lst_duty = list()                                   # Create an empty list
	
		for ch in str_duty:                                 # Store each char of the string in the list
			lst_duty.append(ch)

		lst_duty.append('3')                                # Format according to the protocol the new message
		lst_duty.append('1')
 
		data = ''.join(lst_duty)                            # Convert the list to a string
	
		ser.write(data.encode())                            # Send the new message over UART
		
		return data                                         # Return the data for error checking


