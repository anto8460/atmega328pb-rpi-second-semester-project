
import socket
import serial_com
import select
from time import sleep




TCP_IP   = '169.254.158.77'
TCP_PORT = 4444
BUFFER_SIZE = 1024

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)  # Create a socket
s.bind((TCP_IP, TCP_PORT))
s.listen(5)

conn, addr = s.accept()
print('Connection established!')

def sendADC():                                         # Define a function that uses UART to 
		data = serial_com.getADC()                     # send ADC request and TCP socket to 
		conn.sendall(data)                             # return the received data
		sleep(0.4)


if __name__ == "__main__":
	

	while True:

		try:


			print('Waiting for data...')
			data = 'empty'
            
			ready = select.select([conn], [], [], 0.5)              # Set timeout for the recieve method
			if ready[0]:                                            # In order to not stop the flow execution                    
				data = conn.recv(5, socket.MSG_WAITALL).decode()
				print('Received data: ' + data)

			if (data == '00111' ):                                  # Checks if the User wants LED 1 ON
				data1 = serial_com.LED1_func(1)                     # Turns on LED1 and stores the status in data1
				print(data1)                                        # prints data1
				conn.sendall(data1)                                 # Send the stored data over TCP
				sleep(0.5)

			elif (data == '00011'):                                 # Checks if User wants LED 1 OFF
				data1 = serial_com.LED1_func(0)
				conn.sendall(data1)
				sleep(0.5)
 
			elif (data == '00112'):                                 # Check if User wants LED 2 ON
				data1 = serial_com.LED2_func(1)
				conn.sendall(data1)
				sleep(0.5)

			elif (data == '00012'):                                 # Checks if User wants LED 2 OFF
				data1 = serial_com.LED2_func(0)
				conn.sendall(data1)
				sleep(0.5)

			elif (data[3] == '3'):                                  # Check if the User has send a new Duty Cycle value
				lst_data = list()                                   # Creates an empty list
				for ch in range(3):                                 # Iterates three times to save the first 3 bytes
					lst_data.append(data[ch])                       # of data and stores the value in 'pwm'
				pwm = ''.join(lst_data)                             
				data1 = serial_com.setPWM(pwm)                      # Then changes the duty cycle
				conn.sendall(data1.encode())                        # Returns the new Duty Cycle
				sleep(0.5)

			sendADC()                                               # After a timeout or completing one if statement
                                                                    # requests ADC value
		except (KeyboardInterrupt, IndexError):
			s.close()
			print("Socket Closed!") 
			break

