/*
 * PWM_Functions.h
 *
 * Created: 31/03/2020 20.45.46
 *  Author: Toni
 */ 


#ifndef PWM_FUNCTIONS_H_
#define PWM_FUNCTIONS_H_

extern int duty_cycle;

void setup_PWM(void);
void set_dutyCycle(int duty_cycle);
int getDuty_cycle(void);

#endif /* PWM_FUNCTIONS_H_ */