/*
 * UART_Functions.h
 *
 * Created: 31/03/2020 21.02.50
 *  Author: Toni
 */ 


#ifndef UART_FUNCTIONS_H_
#define UART_FUNCTIONS_H_

#define FOSC 16000000 // Clock speed
#define BAUD 9600
#define MYUBRR FOSC/16/BAUD-1

#define BUFFER_SIZE 5
extern char RX_buffer[BUFFER_SIZE];

void setup_UART(void);
void transmit_UART(char);
void clearRX_buffer(void);





#endif /* UART_FUNCTIONS_H_ */