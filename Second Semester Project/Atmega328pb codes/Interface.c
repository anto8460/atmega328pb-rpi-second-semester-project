/*
 * Interface.c
 *
 * Created: 02/04/2020 10.51.56
 *  Author: Toni
 */ 

#include "Interface.h"
#include "UART_Functions.h"
#include "PWM_Functions.h"
#include <avr/io.h>
#include <util/delay.h>
#include <stdlib.h>



void setup_Interface(void){
	DDRB  |= 0x2C; // 0b0000 0000 >> 0b0010 1100 Configure PC0 and PC1 as input and PC2 and PC3 as output and PB5
	PORTB = 0x03;  //0b0000 0000 >> 0b0000 0011 Activate Pull up resistors on PC0 and PC1
}


void hardware_input(void){
	
	if (!(PINB & 0x01)){ // if !( 0000 0001 and 0000 0001 == 1)
		toggleLED_1();
		//_delay_ms(500);
	}
	else if (!(PINB & 0x02)){
		toggleLED_2();
		//_delay_ms(500);
	}
	
}

void toggleLED_1(void){
	PORTB ^= 0x04; // 0b0000 0000 or 0b000 0100
}

void toggleLED_2(void){
	PORTB ^= 0x08;
}


void send_LED1status(void){
	
	char *data = (char*)calloc(5,sizeof(char));  // Dynamically allocate 4 bytes of data for char and initialize them
	
	if (PORTB & 0x04){
	     data[0] = '1'; //On
		 data[1] = '1'; //LED
		 data[2] = '1'; //LED1
		 data[3] = '\r';
		 data[4] = '\n';
	}
	else{
		 data[0] = '0'; //Off
         data[1] = '1'; //LED
		 data[2] = '1'; //LED1
		 data[3] = '\r';
		 data[4] = '\n';
	}
	
	for (int i = 0; i < 5; i++){
		transmit_UART(data[i]);
	}
	free(data);                      // Dynamicallly erase the data
	UART_LED();
}

void send_LED2status(void){
	
	char *data = (char*)calloc(5,sizeof(char));
	
	if (PORTB & 0x08){
		data[0] = '1'; //On
		data[1] = '1'; //LED
		data[2] = '2'; //LED2
		data[3] = '\r';
		data[4] = '\n';
	}
	else{
		data[0] = '0'; //Off
		data[1] = '1'; //LED
		data[2] = '2'; //LED2
		data[3] = '\r';
		data[4] = '\n';
		
	}
	
	for (int i = 0; i < 5; i++){
		transmit_UART(data[i]);
	}
	free(data);
	UART_LED();
}

void state_LED1(int x)
{
	if(x == 0)
	{
		PORTB &= 0xFB; // 1111 1011
	}
	else if(x == 1)
	{
		PORTB |= 0x04;
	}
}

void state_LED2(int x)
{
	if(x == 0)
	{
		PORTB &= 0xF7 ; // 1111 0111
	}
	else if(x == 1)
	{
		PORTB |= 0x08; // 
	}
}

void controlLED1_UART()
{
	if ((RX_buffer[3] == '1') && (RX_buffer[4] == '1'))       // Manipulate LED over UART
	{
		if(RX_buffer[2] == '1')
		{
			state_LED1(1);
			send_LED1status();
			clearRX_buffer();
		}
		else if (RX_buffer[2] == '0')
		{
			state_LED1(0);
			send_LED1status();
			clearRX_buffer();
		}
	}
}

void controlLED2_UART(void)
{
	        
	 if ((RX_buffer[3] == '1') && (RX_buffer[4] == '2'))
	 {
		  if(RX_buffer[2] == '1')
		  {
			  state_LED2(1);
			  send_LED2status();
			  clearRX_buffer();

		  }
		  else if (RX_buffer[2] == '0')
		  {
			  state_LED2(0);
			  send_LED2status();
			  clearRX_buffer();

		  }
	}
}

void UART_LED(void){
	PORTB ^= 0x20; // 0010 0000 Toggle on Board LED
	//_delay_ms(250);
	//PORTB ^= 0x20;

}