/*
 * UART_Functions.c
 *
 * Created: 31/03/2020 21.03.17
 *  Author: Toni
 */ 

#include "Interface.h"
#include "UART_Functions.h"
#include "PWM_Functions.h"
#include <util/delay.h>
#include <avr/interrupt.h>
#include <avr/io.h>


char RX_buffer[BUFFER_SIZE] = {'\0','\0','\0','\0','\0'};
int writePos = 0;


void clearRX_buffer(void)
{
	for(int i = 0; i < BUFFER_SIZE; i++)
	{
		RX_buffer[i] = '\0';
		//UART_LED();
	}
}

void setup_UART(void)
{

	
	UBRR0H |= (uint8_t)(MYUBRR>>8);                      // Set the HIGH bits of the UART BAUD Rate Register to 0000 0000
	UBRR0L |= (uint8_t)(MYUBRR);                         // Set the LOW bits of the UART BAUD Rate Register  to 0110 0111
		
	UCSR0B |= (1<<TXEN0);								 // Enable USART TX
	UCSR0B |= (1<<RXEN0);								 // Enable USART RX
	UCSR0B |= (1<<RXCIE0);							     // Enable Interrupts
	
	UCSR0C |= (3<<UCSZ00);                               // 8 bit transfer

	//UCSR0C |= (1<<UPM00) | (1<<UPM01);                 // Set parity Odd
	
	sei();
	
}

void transmit_UART(char data)
{
	while ( ! (UCSR0A & (1<<UDRE0)));           // Wait for empty transmit buffer
	
	UDR0 = data;                                // Transmit Data
	//UART_LED();
}

/*char recieve_UART(void)
{
	while (!(UCSR0A & (1<<UDRE0)));
	
	return UDR0;
	
}*/


ISR(USART0_RX_vect){
                                               // Interrupt for receiveing data
	RX_buffer[writePos] = UDR0;
	//transmit_UART(RX_buffer[writePos]);
	writePos++;
	
	
	if (writePos >= BUFFER_SIZE)
	{
		writePos=0;
	}	
	//UART_LED();
}