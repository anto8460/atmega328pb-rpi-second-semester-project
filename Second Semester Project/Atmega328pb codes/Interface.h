/*
 * Interface.h
 *
 * Created: 02/04/2020 10.52.10
 *  Author: Toni
 */ 


#ifndef INTERFACE_H_
#define INTERFACE_H_

void setup_Interface(void);
void hardware_input(void);
void toggleLED_1(void);
void toggleLED_2(void);
void send_LED1status(void);
void send_LED2status(void);
void state_LED1(int x);    // Turn ON or OFF the LED based on input
void state_LED2(int x);    // Turn ON or OFF the LED based on input

void controlLED1_UART(void);
void controlLED2_UART(void);

void UART_LED(void); // Toggle on Board LED when transmitting and receiving


#endif /* INTERFACE_H_ */