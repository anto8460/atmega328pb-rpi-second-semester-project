/*
 * ADC_Functions.h
 *
 * Created: 31/03/2020 20.54.22
 *  Author: Toni
 */ 


#ifndef ADC_FUNCTIONS_H_
#define ADC_FUNCTIONS_H_


void setup_ADC(void);
void start_ADC(void);
void send_ADC(void);

#endif /* ADC_FUNCTIONS_H_ */