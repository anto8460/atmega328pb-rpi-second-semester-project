/*
 * PWM_Functions.c
 *
 * Created: 31/03/2020 20.46.21
 *  Author: Toni
 */ 

#include <avr/io.h>
#include <avr/interrupt.h>
#include <stdio.h>
#include <stdlib.h>
#include "UART_Functions.h"
#include "PWM_Functions.h"
#include "Interface.h"


int duty_cycle = 0;
char send_cycle[3];

void setup_PWM(void){
	
	DDRD |= (1<<PORTD6);                          // Make PD6(OC0A) as output
	
	TCCR0A |= (1<<COM0A1);                        // Set clear OC0A on compare match
	TCCR0A |= (1<<WGM00) | (1<<WGM01);            // Set the timer to FAST-PWM
	
	//TIMSK0 |= (1<<TOIE0);                       // Raise an interrupt on overflow
	
	//sei();
	
	TCCR0B |= (1<<CS00) | (1<<CS02);              // Set the prescaler to 1024 . This starts the timer
	
}


void set_dutyCycle(int duty_cycle){
	OCR0A = duty_cycle;	
}

int getDuty_cycle(void)
{
	char data[3] = {'0', '0', '0'};      
	
		data[0] = RX_buffer[0];                      // store the data of the duty cycle
		data[1] = RX_buffer[1];
		data[2] = RX_buffer[2];
		
		duty_cycle = atoi(data);  // Convert the chars to integer
		
		clearRX_buffer();
		
		return duty_cycle;
		

	UART_LED();
}