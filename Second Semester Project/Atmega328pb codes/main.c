/*
 * Second Semester Project.c
 *
 * Created: 31/03/2020 20.44.31
 * Author : Toni
 */ 

#include <avr/io.h>
#include <util/delay.h>
#include "ADC_Functions.h"
#include "PWM_Functions.h"
#include "UART_Functions.h"
#include "Interface.h"

unsigned long int cycle = 0;

int main(void)
{
	setup_Interface();
	setup_PWM();
	setup_UART();
	setup_ADC();
	start_ADC();
	
	
    while (1) 
    {		
		hardware_input();               // Check for Hardware input
		
//======================================
	
		if (RX_buffer[3] == '2')        // Send ADC on requests
		{
			send_ADC();
			clearRX_buffer();
		}
		
//=====================================	

			controlLED1_UART();         // Control LEDs over UART
			controlLED2_UART();           

//=====================================
		if (RX_buffer[3] == '3' && RX_buffer[4] == '1')
		{
			set_dutyCycle(getDuty_cycle());  // Set the PWM
		}
//==========================================================
		if(cycle >= 100000)                  // Turn Off the LED after 100 000 cycles 
		{	
			if (PORTB & 0x20)
			{
				UART_LED();
			}
			cycle = 0;
		}
		cycle++;
//==========================================================
	}
}

