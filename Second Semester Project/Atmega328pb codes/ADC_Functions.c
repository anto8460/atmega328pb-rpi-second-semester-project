/*
 * ADC_Functions.c
 *
 * Created: 31/03/2020 20.54.06
 *  Author: Toni
 */ 
#include <stdio.h>
#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>
#include "ADC_Functions.h"
#include "PWM_Functions.h"
#include "UART_Functions.h"
#include "Interface.h"

char adc_conv[8] = {'o', 'o', 'o', 'o','o', 'o', 'o', 'o' };  // Assign the arrays with values for easier conversion latter in the communication

//int adc_change = 0;

void start_ADC(void)
{
	ADCSRA |= (1<<ADSC);
}


void setup_ADC(void)
{
	 
	 ADMUX  |= (1<<REFS0);      // Select the voltage reference to be Vcc
	 //ADMUX  |= (1<<ADLAR);      // Select 8 bit resolution 
	 ADCSRA |= (1<<ADEN);       // Enable ADC
	 ADCSRA |= (1<<ADATE);      // Enable auto trigger
//	 ADCSRA |= (1<<ADIE);       // Enable interrupt on conversion completion
	 ADCSRA |= (1<<ADPS0) | (1<<ADPS1) | (1<<ADPS2); // Set prescaler to 128
	 DIDR0  |= (1<<ADC0D);      // Disable digital input on PC0
	 
	 start_ADC();
	// sei();                     // Enable interrupts
	 
}

void send_ADC(void){
	
	
<<<<<<< HEAD
	//if (adc_change != ADC){               // Send data only when ADC is changed
=======
	//if (adc_change != ADC)							// Send data only when ADC is changed
	//{               			
>>>>>>> 9805df1ec5e6311b99e408c2993dc41986b58414
	
		//adc_change = ADC;
		
		sprintf(adc_conv, "%d", ADC);	  // Used to convert the integer to a string
		adc_conv[4] = '2';                // Indicating ADC value >> 2
		adc_conv[5] = '1';                // number of sensor >> 1
		adc_conv[6] = '\r';
		adc_conv[7] = '\n';                // Add a ending char to separate the messages
		for (int i = 0 ; i < 8 ; i++){    // Transmit the digits accordingly
			transmit_UART(adc_conv[i]);
			
		}
		UART_LED();
	//}

}