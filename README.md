# **UART and TCP/IP Communication - Second Semester Project**


## **Description:**

This project is about creating a system of 3 devices that communicate
between each other using different ways of data transmission

The first device is an [ATmega328pb xPlained board](https://www.microchip.com/DevelopmentTools/ProductDetails/atmega328pb-xmini)
which is used as a measuring device(measures analog signals) and also as a controlling device (turning ON/OFF LEDs and outputting PWM).
This device communicates using UART communication.

The second device is a [Raspberry Pi 3b+](https://www.raspberrypi.org/products/raspberry-pi-3-model-b-plus/) which sends and recieves 
information and commands to the *xPlained Board* using UART communication. The *Raspberry Pi* acts like a gateway for the third device and
cummunicates using TCP/IP sockets.

The third device is a regular laptop with Node-Red installed that sends TCP-requests to the Raspberry Pi board in order to controll the *xPlained Board*

[Documentation](https://gitlab.com/anto8460/atmega328pb-rpi-second-semester-project/-/blob/master/Second%20Semester%20Project/Documentation/Second%20Semester%20Project%20Intro.md)


## **Video Presentation:** 

https://www.youtube.com/watch?v=EBPJogWP1_E



![](BlockDiagram.PNG)


